const cacheName = "demo-v1",
	assets = [
		"./",
		"./index.html",
		"./styles.css",
		"./app.js",
		"./offline.webp"
	];

let shouldClaim = false;

self.addEventListener("message", (event) => {
	const { data } = event;
	if (data === "skipWaiting") {
		self.skipWaiting();
	} else if (data === "claim") {
		self.clients.claim();
	}
}, { passive: true });

self.addEventListener("install", (event) => {
	event.waitUntil((async () => {
		try {
			const cache = await caches.open(cacheName);
			return cache.addAll(assets);
		} catch (err) { console.error(err); }
	})());
	
	shouldClaim = registration.active === null;
}, { passive: true });

self.addEventListener("activate", (event) => {
	event.waitUntil((async () => {
		try {
			const keys = await caches.keys();
			return Promise.all(keys.map((key) => {
				if (key !== cacheName) {
					return caches.delete(key);
				}
			}));
		} catch (err) { console.error(err); }
	})());
	
	if (shouldClaim) {
		event.waitUntil(self.clients.claim());
	}
}, { passive: true });

self.addEventListener("fetch", (event) => {
	event.respondWith((async () => {
		if (event.request.mode === "navigate" && event.request.method === "GET" && self.registration.waiting && (await clients.matchAll()).length < 2) {
			self.registration.waiting.postMessage("skipWaiting");
			return new Response("", { headers: { refresh: "0" } });
		}
		
		// Network First
		try {
			return fetch(event.request)
				.then(async (response) => {
					try {
						const cache = await caches.open(cacheName),
							url = new URL(event.request.url),
							scope = new URL(self.registration.scope);
						
						if (url.pathname.startsWith(scope.pathname)) {
							const stripped = url.pathname.substring(scope.pathname.length);
							
							// Only cache needed assets
							if (assets.includes(`./${stripped}`)) {
								cache.put(event.request, response.clone());
							}
						}
					} catch (err) {
					} finally {
						return response;
					}
				})
				.catch(async (err) => {
					try {
						const cache = await caches.open(cacheName);
						return cache.match(event.request);
					} catch (err) {
						return false;
					}
				})
				.then((match) => match ? match : new Response("", { status: 500 }));
		} catch (err) { throw err; }
	})());
}, { passive: true });
