window.addEventListener("load", () => {
	const img = document.getElementById("answerImg"),
		span = document.getElementById("answer"),
		handleOnline = () => {
			img.style.visibility = "hidden";
			span.textContent = "";
			
			fetch("https://yesno.wtf/api")
				.then((response) => {
					if (response.ok) {
						const contentType = response.headers.get("content-type");
						if (contentType?.includes("application/json")) {
							return response.json();
						}
					}
					throw new Error("Network error");
				})
				.then((json) => {
					img.addEventListener("load", () => {
						img.style.visibility = "";
						span.textContent = json.answer;
					}, { once: true, passive: true });
					img.src = json.image;
				})
				.catch((err) => {
					img.src = "offline.webp";
					span.textContent = "";
				});
		},
		handleOffline = () => {
			img.src = "offline.webp";
			span.textContent = "";
		};
		
		if (navigator.onLine) {
			handleOnline();
		} else {
			handleOffline();
		}
		
		window.addEventListener("offline", handleOffline, { passive: true });
		window.addEventListener("online", handleOnline, { passive: true });
}, { once: true });

navigator?.serviceWorker?.register("./sw.js")
	.then((registration) => {
		let refreshing = false;
		
		if (!registration) {
			return;
		}
		
		const { active, installing, waiting } = registration;
		
		if (
			active &&
			active.state === "activated" &&
			!waiting &&
			!installing
		) {
			active.postMessage("claim");
		}
		
		navigator.serviceWorker.addEventListener("controllerchange", (event) => {
			const { controller } = event.target;
			
			if (controller?.state === "activating") {
				if (!refreshing) {
					refreshing = true;
					window.location.reload();
				}
			}
		}, { passive: true });
		
		const promptToRefresh = () => {
			if (registration.active !== null) {
				const nvAlert = document.querySelector(".new-version-alert") ?? false;
				if (nvAlert) {
					nvAlert.dataset.active = true;
					nvAlert?.addEventListener("click", (event) => {
						registration.waiting.postMessage("skipWaiting");
					}, { once: true, passive: true });
				}
			}
		};
		
		if (waiting) {
			return promptToRefresh();
		}
		
		const awaitStateChange = (event) => {
			const { state } = event.target;
			if (state === "installed") {
				promptToRefresh();
			}
		};
		
		registration.addEventListener("updatefound", (event) => {
			const { installing } = event.target;
			installing?.addEventListener("statechange", awaitStateChange, { passive: true });
		}, { passive: true });
	});
