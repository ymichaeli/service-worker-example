# Service Worker Example

This webapp provides an example for using ServiceWorkers in a webpage to create PWAs and cache content when the device goes offline.

## What it does

The app makes use of the Yes or No API to generate random content in the app. If the device is offline, a graphic indicating that the device is currently offline will be shown instead.

## How it works

The ServiceWorker initially caches the app content to make it immediately available while offline. It then employs a network-first caching strategy. This means that it will first reach out to the network to fetch resources. If the resource is found, it will then update the cache. If the resource is not found, it will check the cache to see if the resource is available there, otherwise a 500 response is sent back to the browser.

## Features

### CSS
- Aspect Ratio
- Flexbox
- Font-family keyword `system-ui`
- Logical properties

### JavaScript
- Arrow Functions
- Fetch API
- Online / Offline status
- Service Workers

## Supported Browsers

Modern versions of:
- Google Chrome
- Microsoft Edge
- Mozilla Firefox
- Opera